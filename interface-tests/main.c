#include <gtk/gtk.h>
#include "teleport-app.h"

int main (int argc, char *argv[])
{
	g_type_init ();
	gtk_init (&argc, &argv);

	TeleportApp *app = teleport_app_new ();

	gtk_main ();
	return 0;
}
