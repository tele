#ifndef _TELEPORT_CAIRO_H
#define _TELEPORT_CAIRO_H

#include <cairo.h>
#include <glib.h>

/* from MacSlow's cairo-utils.c */
void
cairo_draw_round_rect (cairo_t* pContext,
		double fAspectRatio, /* aspect-ratio */
		double fX, /* top-left corner */
		double fY, /* top-left corner */
		double fCornerRadius, /* "size" of the corners */
		double fWidth, /* width of the rectangle */
		double fHeight /* height of the rectangle */);
#endif
