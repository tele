#ifndef _TELEPORT_APP
#define _TELEPORT_APP

#include <glib-object.h>
#include "teleport-display.h"

G_BEGIN_DECLS

#define TELEPORT_APP_TYPE_TELEPORT_APP teleport_app_get_type()

#define TELEPORT_APP_TELEPORT_APP(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  TELEPORT_APP_TYPE_TELEPORT_APP, TeleportApp))

#define TELEPORT_APP_TELEPORT_APP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  TELEPORT_APP_TYPE_TELEPORT_APP, TeleportAppClass))

#define TELEPORT_APP_IS_TELEPORT_APP(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  TELEPORT_APP_TYPE_TELEPORT_APP))

#define TELEPORT_APP_IS_TELEPORT_APP_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  TELEPORT_APP_TYPE_TELEPORT_APP))

#define TELEPORT_APP_TELEPORT_APP_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  TELEPORT_APP_TYPE_TELEPORT_APP, TeleportAppClass))

typedef struct {
  GObject parent;
} TeleportApp;

typedef struct {
  GObjectClass parent_class;
} TeleportAppClass;

GType teleport_app_get_type (void);

TeleportApp* teleport_app_new (void);

G_END_DECLS

#endif /* _TELEPORT_APP */
