/* teleport-display.h */

#ifndef _TELEPORT_DISPLAY
#define _TELEPORT_DISPLAY

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TELEPORT_TYPE_DISPLAY teleport_display_get_type()

#define TELEPORT_DISPLAY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  TELEPORT_TYPE_DISPLAY, TeleportDisplay))

#define TELEPORT_DISPLAY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  TELEPORT_TYPE_DISPLAY, TeleportDisplayClass))

#define TELEPORT_IS_DISPLAY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  TELEPORT_TYPE_DISPLAY))

#define TELEPORT_IS_DISPLAY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  TELEPORT_TYPE_DISPLAY))

#define TELEPORT_DISPLAY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  TELEPORT_TYPE_DISPLAY, TeleportDisplayClass))

typedef struct {
  GtkDrawingArea parent;
} TeleportDisplay;

typedef struct {
  GtkDrawingAreaClass parent_class;
} TeleportDisplayClass;

GType teleport_display_get_type (void);

GtkWidget* teleport_display_new (void);

G_END_DECLS

#endif /* _TELEPORT_DISPLAY */
