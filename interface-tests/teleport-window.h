/* teleport-window.h */

#ifndef _TELEPORT_WINDOW
#define _TELEPORT_WINDOW

#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define TELEPORT_TYPE_WINDOW teleport_window_get_type()

#define TELEPORT_WINDOW(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), \
								 TELEPORT_TYPE_WINDOW, TeleportWindow))

#define TELEPORT_WINDOW_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), \
							  TELEPORT_TYPE_WINDOW, TeleportWindowClass))

#define TELEPORT_IS_WINDOW(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
								 TELEPORT_TYPE_WINDOW))

#define TELEPORT_IS_WINDOW_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), \
							  TELEPORT_TYPE_WINDOW))

#define TELEPORT_WINDOW_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), \
								TELEPORT_TYPE_WINDOW, TeleportWindowClass))

typedef struct {
	GtkWindow parent;
} TeleportWindow;

typedef struct {
	GtkWindowClass parent_class;
} TeleportWindowClass;

GType teleport_window_get_type (void);

GtkWidget* teleport_window_new (void);

G_END_DECLS

#endif /* _TELEPORT_WINDOW */
