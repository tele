/* teleport-display.c */

#include "teleport-display.h"
#include "teleport-cairo.h"

G_DEFINE_TYPE (TeleportDisplay, teleport_display, GTK_TYPE_DRAWING_AREA)

#define DISPLAY_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), TELEPORT_TYPE_DISPLAY, TeleportDisplayPrivate))

typedef struct _TeleportDisplayPrivate TeleportDisplayPrivate;

struct _TeleportDisplayPrivate
{
};

static void
teleport_display_finalize (GObject *object)
{
  G_OBJECT_CLASS (teleport_display_parent_class)->finalize (object);
}

static gboolean
teleport_display_expose (GtkWidget *widget, GdkEventExpose *event)
{
	cairo_t *cr = gdk_cairo_create (widget->window);
	int width = widget->allocation.width;
	int height = widget->allocation.height;

	g_print ("expose\n");

	cairo_set_source_rgba (cr, 143.0/255, 86.0/255, 201.0/255, 0.8);
	cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
	cairo_paint (cr);

	/* first icon */

	cairo_set_source_rgba (cr, 0.8, 0.8, 0.8, 0.6);
	cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
	cairo_draw_round_rect (cr, 1.0, 10, 0, 3, 138, 138);
	cairo_fill_preserve (cr);	

	cairo_set_source_rgba (cr, 0.6, 0.6, 0.6, 1.0);
	cairo_stroke (cr);

	GdkPixbuf *pix = gtk_icon_theme_load_icon (gtk_icon_theme_get_default(), "document-open", 96, 0, NULL);
	gdk_cairo_set_source_pixbuf (cr, pix, 10+138/2-96/2, 138/2-96/2-10);
	cairo_paint (cr);
	
	/* icon reflection */
	cairo_rectangle (cr, 10, 0, 138, 138);
	cairo_clip (cr);
	GdkPixbuf *reflect = gdk_pixbuf_flip (pix, FALSE);
	gdk_cairo_set_source_pixbuf (cr, reflect, 10+138/2-96/2, 138/2-96/2+96-10);
	cairo_paint_with_alpha (cr, 0.3);
	cairo_reset_clip (cr);

	/* second icon */
	cairo_set_source_rgba (cr, 0.8, 0.8, 0.8, 0.6);
	cairo_set_operator (cr, CAIRO_OPERATOR_OVER);
	cairo_draw_round_rect (cr, 1.0, 158, 0, 3, 138, 138);
	cairo_fill_preserve (cr);


	cairo_set_source_rgba (cr, 0.6, 0.6, 0.6, 1.0);
	cairo_stroke (cr);

	pix = gtk_icon_theme_load_icon (gtk_icon_theme_get_default(), "system-run", 96, 0, NULL);
	gdk_cairo_set_source_pixbuf (cr, pix, 158+138/2-96/2, 138/2-96/2-10);
	cairo_paint (cr);

	/* second icon reflection */
	cairo_rectangle (cr, 158, 0, 138, 138);
	cairo_clip (cr);
	reflect = gdk_pixbuf_flip (pix, FALSE);
	gdk_cairo_set_source_pixbuf (cr, reflect, 158+138/2-96/2, 138/2-96/2+96-10);
	cairo_paint_with_alpha (cr, 0.3);
	cairo_reset_clip (cr);

	cairo_destroy (cr);

	return FALSE;
}

static void
teleport_display_class_init (TeleportDisplayClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_class_add_private (klass, sizeof (TeleportDisplayPrivate));

  object_class->finalize = teleport_display_finalize;

  widget_class->expose_event = teleport_display_expose;
}

static void
teleport_display_init (TeleportDisplay *self)
{
	gtk_widget_set_size_request (GTK_WIDGET(self), 306, 158);
}

GtkWidget*
teleport_display_new (void)
{
  return g_object_new (TELEPORT_TYPE_DISPLAY, NULL);
}
