#include "teleport-cairo.h"

/* from MacSlow's cairo-utils.c */
void
cairo_draw_round_rect (cairo_t* pContext,
		double fAspectRatio, /* aspect-ratio */
		double fX, /* top-left corner */
		double fY, /* top-left corner */
		double fCornerRadius, /* "size" of the corners */
		double fWidth, /* width of the rectangle */
		double fHeight /* height of the rectangle */)
{
	double fRadius = fCornerRadius / fAspectRatio;

	/* top-left, right of the corner */
	cairo_move_to (pContext, fX + fRadius, fY);

	/* top-right, left of the corner */
	cairo_line_to (pContext,
			fX + fWidth - fRadius,
			fY);

	/* top-right, below the corner */
	cairo_arc (pContext,
			fX + fWidth - fRadius,
			fY + fRadius,
			fRadius,
			-90.0f * G_PI / 180.0f,
			0.0f * G_PI / 180.0f);

	/* bottom-right, above the corner */
	cairo_line_to (pContext,
			fX + fWidth,
			fY + fHeight - fRadius);

	/* bottom-right, left of the corner */
	cairo_arc (pContext,
			fX + fWidth - fRadius,
			fY + fHeight - fRadius,
			fRadius,
			0.0f * G_PI / 180.0f,
			90.0f * G_PI / 180.0f);

	/* bottom-left, right of the corner */
	cairo_line_to (pContext,
			fX + fRadius,
			fY + fHeight);

	/* bottom-left, above the corner */
	cairo_arc (pContext,
			fX + fRadius,
			fY + fHeight - fRadius,
			fRadius,
			90.0f * G_PI / 180.0f,
			180.0f * G_PI / 180.0f);

	/* top-left, below the corner */
	cairo_line_to (pContext,
			fX,
			fY + fRadius);

	/* top-left, right of the corner */
	cairo_arc (pContext,
			fX + fRadius,
			fY + fRadius,
			fRadius,
			180.0f * G_PI / 180.0f,
			270.0f * G_PI / 180.0f);
}

