/* teleport-window.c */

#include "teleport-window.h"
#include "teleport-cairo.h"
#include <cairo.h>

G_DEFINE_TYPE (TeleportWindow, teleport_window, GTK_TYPE_WINDOW)

#define WINDOW_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), TELEPORT_TYPE_WINDOW, TeleportWindowPrivate))

typedef struct _TeleportWindowPrivate TeleportWindowPrivate;

struct _TeleportWindowPrivate
{
};

static GtkWindowClass *parent_class;

static void
teleport_window_finalize (GObject *object)
{
	G_OBJECT_CLASS (teleport_window_parent_class)->finalize (object);
}

static gboolean
teleport_window_expose (GtkWidget *widget, GdkEventExpose *event)
{
	cairo_t *cr;
	cr = gdk_cairo_create (widget->window);

	int width = widget->allocation.width;
	int height = widget->allocation.height;

	cairo_scale (cr, (double)width, (double)height);

	cairo_set_source_rgba (cr, 1.0, 1.0, 1.0, 0.0);
	cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
	cairo_paint (cr);


	cairo_set_source_rgba (cr, 143.0/255, 86.0/255, 201.0/255, 0.8);
	cairo_draw_round_rect (cr, 1.0, 0, 0, 0.015, 1.0, 1.0);
	cairo_fill_preserve (cr);

	cairo_destroy (cr);

	return GTK_WIDGET_CLASS (parent_class)->expose_event (widget, event);
}

static void
teleport_window_screen_changed (GtkWidget* widget, GdkScreen* old_screen)
{
	GdkScreen *screen = gtk_widget_get_screen (widget);
	GdkColormap *colormap = gdk_screen_get_rgba_colormap (screen);

	if (!colormap)
	{
		colormap = gdk_screen_get_rgb_colormap (screen);
		g_print ("Unable to get an RGBA colormap\n");
	}

	gtk_widget_set_colormap (widget, colormap);
}

static void
teleport_window_class_init (TeleportWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	g_type_class_add_private (klass, sizeof (TeleportWindowPrivate));
	object_class->finalize = teleport_window_finalize;

	widget_class->expose_event = teleport_window_expose;
	widget_class->screen_changed = teleport_window_screen_changed;
}

static void
teleport_window_init (TeleportWindow *self)
{
	teleport_window_screen_changed (GTK_WIDGET(self), NULL);
	gtk_widget_set_app_paintable (GTK_WIDGET(self), TRUE);
	gtk_widget_add_events (GTK_WIDGET(self), GDK_ALL_EVENTS_MASK);
}

GtkWidget*
teleport_window_new (void)
{
	GtkWindow *window;
	window = g_object_new (TELEPORT_TYPE_WINDOW,
			"type", GTK_WINDOW_TOPLEVEL,
			"decorated", FALSE,
			"default-height", 400,
			"default-width",306,
			"icon-name", "search",
			"title", "Teleport Launcher",
			NULL);

	return GTK_WIDGET(window);
}
