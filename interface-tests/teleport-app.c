/* teleport-app.c */

#include "teleport-app.h"
#include "teleport-window.h"

G_DEFINE_TYPE (TeleportApp, teleport_app, G_TYPE_OBJECT)

#define TELEPORT_APP_PRIVATE(o) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((o), TELEPORT_APP_TYPE_TELEPORT_APP, TeleportAppPrivate))

typedef struct _TeleportAppPrivate TeleportAppPrivate;

struct _TeleportAppPrivate
{
	GtkWidget *window;
	GtkWidget *display;

	GtkWidget *vbox;
};

static void
teleport_app_dispose (GObject *object)
{
	if (G_OBJECT_CLASS (teleport_app_parent_class)->dispose)
		G_OBJECT_CLASS (teleport_app_parent_class)->dispose (object);
}

static void
teleport_app_finalize (GObject *object)
{
	G_OBJECT_CLASS (teleport_app_parent_class)->finalize (object);
}

static void
teleport_app_class_init (TeleportAppClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (TeleportAppPrivate));

	object_class->dispose = teleport_app_dispose;
	object_class->finalize = teleport_app_finalize;
}

static void
teleport_app_init (TeleportApp *self)
{
}

TeleportApp*
teleport_app_new (void)
{
	TeleportApp *app = g_object_new (TELEPORT_APP_TYPE_TELEPORT_APP, NULL);

	TeleportAppPrivate *priv = TELEPORT_APP_PRIVATE (app);

	priv->window = teleport_window_new ();
	priv->vbox = gtk_vbox_new (FALSE, 10);
	priv->display = teleport_display_new ();
	
	gtk_container_add (GTK_CONTAINER(priv->window), priv->vbox);

	gtk_box_pack_start (GTK_BOX(priv->vbox), priv->display, TRUE, TRUE, 10);

	gtk_widget_show_all (priv->window);

	return app;
}
