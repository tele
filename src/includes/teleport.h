/**
*
* Teleport header
*
* teleport.h
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/



#ifndef _HAS_TELEPORT_H
#define _HAS_TELEPORT_H

#include <glib.h>
#include <gmodule.h>

#define TELEPORT_ABI 20070721


typedef struct _TeleportAction TeleportAction;
typedef struct _TeleportPlugin TeleportPlugin;
typedef struct _TeleportPluginVTable TeleportPluginVTable;


typedef gboolean (*InitPluginProc) (const TeleportPlugin*);
typedef void (*FiniPluginProc) (const TeleportPlugin*);
typedef GSList* (*QueryProc) (const TeleportPlugin*, const gchar*);
typedef gboolean (*DoActionProc) (TeleportAction*);
typedef TeleportPluginVTable* (*GetVTableProc) ();
typedef void (*CancelQueryProc) ();

/*
 * A TeleportAction represents a query result from a plugin.
 * Text tipically indicates the text that will appear in the user interface.
 * Backend is a pointer to the plugin which generated the action.
 * Data is a pointer to a private data structure that the plugin can use later.
 * */
struct _TeleportAction
{
	gchar* mime_type;
	gchar* text;
	const TeleportPlugin* backend;

	gpointer data;
};

/*
 * Represents a plugin
 * Queries are executed in order of ascending priority (ie. -1 is executed
 * before 5)
 * modulePtr is a pointer to the GModule handler.
 * vTable is a pointer to the plugin virtuals table, see the next struct.
 * */
struct _TeleportPlugin
{
	gint priority;
	GModule* modulePtr;

	TeleportPluginVTable* vTable;
};

/*
 * A vTable gives the core info on the plugin, name is obviously the name of the
 * plugin *Proc are function pointers to several procedures that plugins need to
 * implement in order to interact with the core, for headers see the definition
 * in the top of this file.
 * */
struct _TeleportPluginVTable
{
	gchar* name;

	InitPluginProc initPlugin;
	FiniPluginProc finiPlugin;

	QueryProc query;
	DoActionProc doAction;
	CancelQueryProc cancelQuery;
};

/* from plugin.c */
TeleportPlugin* teleport_load_plugin (const gchar* name);
void teleport_unload_plugin (TeleportPlugin *plug);
void teleport_traverse_plugins ();
void teleport_perform_query (const gchar *s);
void teleport_do_action (TeleportAction *action);


/* from event.c */
void teleport_trigger_query_update (GSList *data);

#endif
