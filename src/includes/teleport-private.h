/**
*
* Teleport ABI header
*
* teleport-private.h
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#ifndef _HAS_TELEPORT_PRIVATE_H
#define _HAS_TELEPORT_PRIVATE_H

#include <glib.h>

/* gui.c */
gboolean _gui_init (int *argc, char ***argv);
gboolean _gui_update_list (gpointer data);

/* event.c */
gboolean teleport_init_main_loop (void);
gboolean teleport_fini_main_loop (void);

#endif
