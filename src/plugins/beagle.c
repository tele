/**
*
* beagle plugin
*
* beagle.c
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#include <beagle/beagle.h>
#include <teleport.h>

#define PLUGIN_NAME "beagleplugin"

static BeagleClient *client;
static const TeleportPlugin *myself;
static BeagleQuery *beagle_query;

static void
hits_finished (BeagleQuery *q, BeagleFinishedResponse *response, gpointer p)
{
	g_object_unref(q);
}

static void 
beagle_cancel_query(void) 
{
	if (beagle_query) {
		g_object_unref(beagle_query);
		g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s: Last Query Canceled\n", PLUGIN_NAME);
	}
}

static void
hits_added (BeagleQuery *query, BeagleHitsAddedResponse *response) 
{
	GSList *hits, *l;
	GSList *actionGroup = NULL;
	TeleportAction *act;

	hits = beagle_hits_added_response_get_hits (response);

	g_print("------------------------\n");
	for (l = hits; l; l = l->next) 
	{
		BeagleHit *h = BEAGLE_HIT (l->data);
		g_print("[%s] %s\n", beagle_hit_get_mime_type(h), beagle_hit_get_uri(h));
		
		act = g_new (TeleportAction, 1);
		act->text = g_strdup (beagle_hit_get_uri(h));
		act->mime_type = g_strdup (beagle_hit_get_mime_type(h));
		act->backend = myself;
		act->data = NULL;

		actionGroup = g_slist_append (actionGroup, act);
	}
	g_print("-------------------------\n");

	if (actionGroup)
		teleport_trigger_query_update (actionGroup);
}

static 
GSList * beagleSearch (const TeleportPlugin *p, const gchar *q)
{
	BeagleQuery *query = beagle_query_new ();
		
	beagle_query_add_text(query, q);

	g_signal_connect (query, "hits-added", G_CALLBACK(hits_added), client);
	g_signal_connect (query, "finished", G_CALLBACK(hits_finished), NULL);

	beagle_client_send_request_async (client, BEAGLE_REQUEST(query), NULL);

	return NULL;
}

static 
gboolean beagleDoAction (TeleportAction *act)
{
	return TRUE;
}

static
gboolean beagleInitPlugin (const TeleportPlugin *p)
{

	g_type_init ();

	myself = p;

	client = beagle_client_new (NULL);
	
	if (!client)
	{
		g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "Failed to load %s plugin", PLUGIN_NAME);
		
		return FALSE;
	}

	return TRUE;
}

static
void beagleFiniPlugin (const TeleportPlugin *p)
{
	g_object_unref (client);
}

TeleportPluginVTable beagleVTable = {
	(gchar *)PLUGIN_NAME,
	beagleInitPlugin,
	beagleFiniPlugin,
	beagleSearch,
	beagleDoAction,
	beagle_cancel_query
};

int teleport_get_abi ()
{
	    return TELEPORT_ABI;
}

TeleportPluginVTable* teleport_get_vtable () 
{
	return &beagleVTable;
}

