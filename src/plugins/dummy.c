#include "teleport.h"

static
GList* dummySearch (const TeleportPlugin *plug, const gchar* term)
{
	g_print ("This is just a dummy search.\n");
	return NULL;
}

static
gboolean dummyDoAction (TeleportAction *act)
{
	g_print ("I would do something but I'm just a stupid plugin\n");
	return FALSE;
}

static
gboolean dummyInitPlugin (const TeleportPlugin* plug)
{
	g_print ("Dummy: performing initialization.\n");
	return TRUE;
}

static
void dummyFiniPlugin (const TeleportPlugin* plug)
{
	g_print ("Dummy: unloading.\n");
}

static
TeleportPluginVTable dummyVTable = {
	(gchar*)"dummy",
	dummyInitPlugin,
	dummyFiniPlugin,
	dummySearch,
	dummyDoAction
};

TeleportPluginVTable* teleportGetVTable ()
{
	return &dummyVTable;
}
