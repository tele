/**
*
* run in console plugin
*
* tracker.c
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#include "teleport.h"
/* FIXME this should be handled via dynamic settings */
#define CONSOLE_PROGRAM "gnome-terminal -x"

static
GSList* console_query (const TeleportPlugin *plug, const gchar* term)
{
	GSList* retList = NULL;
	TeleportAction* retAct = g_new (TeleportAction, 1);

	retAct->mime_type = NULL;
	retAct->text = g_strdup(term);
	retAct->backend = plug;
	retAct->data = NULL;

	retList = g_slist_append (retList, retAct);

	return retList;
}

static
gboolean console_do_action (TeleportAction *act)
{
	GString* builder = g_string_new (CONSOLE_PROGRAM);

	g_string_append (builder, " ");
	g_string_append (builder, act->text);

	g_spawn_command_line_async (builder->str, NULL);

	g_string_free (builder, TRUE);

	return FALSE;
}

static
gboolean console_init_plugin (const TeleportPlugin* plug)
{
	return TRUE;
}

static
void console_fini_plugin (const TeleportPlugin* plug)
{
}

static
TeleportPluginVTable consoleVTable = {
	(gchar*)"console",
	console_init_plugin,
	console_fini_plugin,
	console_query,
	console_do_action,
	NULL
};

int teleport_get_abi ()
{
	return TELEPORT_ABI;
}

TeleportPluginVTable* teleport_get_vtable ()
{
	return &consoleVTable;
}
