/**
*
* tracker plugin
*
* tracker.c
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#include "teleport.h"
#include <tracker.h>

#define QUERY_MAX 25
#define PLUGIN_NAME "tracker"

struct _trackerPrivateStruct
{
	TrackerClient *client;
	gboolean query_is_running;
} trackerPrivate;

static
void trackerCallback (GPtrArray *res, GError *error, gpointer user_data)
{
	int i;
	char **metadata;
	TeleportAction *act;
	GSList *actionGroup = NULL;

	if (error)
	{
		g_warning ("Tracker error: %s\n",error->message);
		return;
	}

	for (i = 0; i < res->len; i++)
	{
		metadata = (char**)g_ptr_array_index (res, i);
		if (metadata[0] && metadata[1] && metadata[2])
		{
			act = g_new (TeleportAction, 1);
			act->text = g_strdup (metadata[0]);
			act->mime_type = g_strdup (metadata[2]);
			act->backend = (TeleportPlugin*) user_data;
			act->data = NULL; /* FIXME: of course we have to do something here */
			/* still print something to get feedback */
			g_print ("Tracker result: [%s] %s\n",metadata[2], metadata[0]);
			actionGroup = g_slist_append (actionGroup,act);
		}
		/* TODO: add the acts to a list and call the update function */
	}
	if (actionGroup)
		teleport_trigger_query_update (actionGroup);
	g_ptr_array_free (res, TRUE);

	trackerPrivate.query_is_running = FALSE;
}

static
GSList* trackerQuery (const TeleportPlugin *plug, const gchar* term)
{
	
	if (trackerPrivate.client) 
	{
		trackerPrivate.query_is_running = TRUE;
		tracker_search_text_detailed_async (trackerPrivate.client, -1, SERVICE_FILES, term, 0, QUERY_MAX, trackerCallback, (gpointer)plug);
	} else
	{
		g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s: not connect do daemon\n", PLUGIN_NAME);
	}

	return NULL;
}

static
gboolean trackerDoAction (TeleportAction *act)
{
	return FALSE;
}

static
gboolean trackerInitPlugin (const TeleportPlugin* plug)
{
	
	/* FIXME: this call returns something even if tracker is not running */
	trackerPrivate.client = tracker_connect (TRUE); /* FIXME: this should be false */

	if (!trackerPrivate.client)
		return FALSE;

	trackerPrivate.query_is_running = FALSE;

	return TRUE;
}

static
void trackerFiniPlugin (const TeleportPlugin* plug)
{
	tracker_disconnect (trackerPrivate.client);
}

static void
tracker_cancel_query (void)
{
	if (trackerPrivate.query_is_running) 
	{
		tracker_disconnect (trackerPrivate.client);
		trackerPrivate.client = tracker_connect (FALSE);
		trackerPrivate.query_is_running = FALSE;

		g_log (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, "%s: Last Call Canceled\n", PLUGIN_NAME);
	}
}

static
TeleportPluginVTable trackerVTable = {
	(gchar*)PLUGIN_NAME,
	trackerInitPlugin,
	trackerFiniPlugin,
	trackerQuery,
	trackerDoAction,
	tracker_cancel_query
};

int teleport_get_abi ()
{
	return TELEPORT_ABI;
}

TeleportPluginVTable* teleport_get_vtable ()
{
	return &trackerVTable;
}
