/**
*
* Teleport plugin management
*
* plugin.c
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#include "teleport-private.h"
#include "teleport.h"

GList *pluginList = NULL;

TeleportPlugin *
teleport_load_plugin (const gchar* name)
{
	TeleportPlugin *plug; 
	GModule *dlm;
	gchar* pluginPath;

	g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: Attempting to load", name);

	if (!g_module_supported())
	{
		g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: This platform doesn't support modules", name);
		return NULL;
	}

	plug = g_new (TeleportPlugin, 1);

	pluginPath = g_module_build_path ("plugins",name);

	g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: Loading %s", name, pluginPath);

	dlm = g_module_open (pluginPath, G_MODULE_BIND_LAZY);
	if (dlm)
	{
		GetVTableProc getVTable;
		int (*getAbi)();

		g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: Loaded", name);

		if (!g_module_symbol(dlm, "teleport_get_abi", (gpointer)(&getAbi))
			|| (*getAbi)() != TELEPORT_ABI)
		{
			g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: ABI mismatch, recompile the plugin", name);
			g_module_close (dlm);
			g_free (pluginPath);
			g_free (plug);

			return NULL;

		}

		if (!g_module_symbol(dlm, "teleport_get_vtable", (gpointer)(&getVTable)))
		{
			g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: Unable to find teleportGetVTable", name);
			g_module_close (dlm);
			g_free (pluginPath);
			g_free (plug);

			return NULL;
		}


		plug->priority = 0;
		plug->modulePtr = dlm;
		plug->vTable = (*getVTable) ();

		g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: VTable loaded", name);

		if (!(*plug->vTable->initPlugin) (plug))
		{
			g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: unable to init plugin",name);
			g_module_close (dlm);
			g_free (pluginPath);
			g_free (plug);
			return NULL;
		}

		pluginList = g_list_append (pluginList, plug);
	}
	else {
		g_log (NULL, G_LOG_LEVEL_DEBUG, "%s: Unable to load %s", name,pluginPath);
		g_free (plug);
	}

	g_free (pluginPath);

	return plug;
}

void
teleport_unload_plugin (TeleportPlugin *plug)
{
	(*plug->vTable->finiPlugin) (plug);

	pluginList = g_list_remove (pluginList, (gpointer)plug);

	g_module_close (plug->modulePtr);

	free (plug);
}

void
teleport_traverse_plugins ()
{
	GList *iterator;
	for (iterator = pluginList; iterator; iterator = iterator->next)
	{
		TeleportPlugin *plug = (TeleportPlugin*)iterator->data;
		g_print ("Priority: %d\n",plug->priority);
		g_print ("Loaded: %s\n",plug->vTable->name);
	}
}

void 
teleport_perform_query (const gchar *s)
{
	GList *iterator;
	GSList *queryResult;
	for (iterator = pluginList; iterator; iterator = iterator->next)
	{
		TeleportPlugin *plug = (TeleportPlugin*)iterator->data;
		
		if (plug->vTable->cancelQuery)
			(*plug->vTable->cancelQuery)();

		queryResult = (*plug->vTable->query) (plug, s);
		if (queryResult) /* Possible sync returns */
		{
			teleport_trigger_query_update (queryResult);
		}
	}
}

void
teleport_do_action (TeleportAction *action)
{
	(*(action->backend->vTable->doAction))(action);
}
