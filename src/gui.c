/**
*
* Teleport graphical interface
*
* gui.c
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#include <gtk/gtk.h>
#include <glib.h>
#include <glade/glade.h>
#include <string.h>
#include "teleport-private.h"
#include "teleport.h"

#define MAIN_WINDOW_GLADE_FILE  "glade/gui.glade"

static GtkWidget *gMainWindow = NULL;
static GtkWidget *gQueryWidget = NULL;
static GtkWidget *gResultsTable = NULL;


enum
{
	COL_RESULT = 0,
	COL_BACKEND,
	NUM_COLS
};

static void _gui_add_item (gpointer data, gpointer userdata)
{
	TeleportAction *action = (TeleportAction*)data;
	GtkListStore *pTreeStore = GTK_LIST_STORE (gtk_tree_view_get_model(GTK_TREE_VIEW(gResultsTable)));
	GtkTreeIter iter;

	GSList *list = (GSList*)g_object_get_data (G_OBJECT(pTreeStore), "private-list");
	list = g_slist_append (list, data);
	g_object_set_data (G_OBJECT(pTreeStore), "private-list", list);

	gtk_list_store_append (pTreeStore, &iter);
	gtk_list_store_set (pTreeStore, &iter,
			COL_RESULT, action->text,
			COL_BACKEND, action->backend->vTable->name,
			-1);
}

gboolean _gui_update_list (gpointer data)
{
	g_slist_foreach ((GSList*)data, _gui_add_item, NULL);
	return FALSE;
}

static void _gui_search (GtkButton *pButton, gpointer data)
{
	const char *query = gtk_entry_get_text(GTK_ENTRY(gQueryWidget));
	GtkListStore *pTreeStore = GTK_LIST_STORE (gtk_tree_view_get_model(GTK_TREE_VIEW(gResultsTable)));

	if (strlen(query) != 0) {
		gtk_list_store_clear (pTreeStore);
		g_object_set_data (G_OBJECT(pTreeStore), "private-list", NULL);
		teleport_perform_query (query);
	}
}

static gboolean _gui_tree_view_event_handler (GtkWidget* widget, GdkEventButton* event, gpointer data)
{
	switch (event->type)
	{
		case GDK_2BUTTON_PRESS:
			{
				GtkTreePath *path;
				GtkTreeIter iter;
				GtkTreeModel *pTreeModel = gtk_tree_view_get_model(GTK_TREE_VIEW(gResultsTable));

				g_print ("double click %.2f, %.2f\n",event->x, event->y);
				if (gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW(widget), (gint)event->x, (gint)event->y, &path, NULL, NULL, NULL))
				{
					gint *ind = gtk_tree_path_get_indices (path);
					TeleportAction *action;
					GSList *list = (GSList*)g_object_get_data (G_OBJECT(pTreeModel), "private-list");
					action = (TeleportAction*)(g_slist_nth_data (list,ind[0]));
					teleport_do_action (action);
				}
				break;
			}
	}

	return FALSE;
}

static void _gui_init_tree_view ()
{
	GtkCellRenderer *pCellRenderer;
	GtkListStore *pTreeStore;

	pCellRenderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gResultsTable), -1, "Result", pCellRenderer, "text", COL_RESULT, NULL);
	pCellRenderer = gtk_cell_renderer_text_new ();
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (gResultsTable), -1, "Backend", pCellRenderer, "text", COL_BACKEND, NULL);

	pTreeStore = gtk_list_store_new (NUM_COLS, G_TYPE_STRING, G_TYPE_STRING);

	gtk_tree_view_set_model (GTK_TREE_VIEW (gResultsTable), GTK_TREE_MODEL(pTreeStore));

	gtk_widget_add_events (GTK_WIDGET (gResultsTable), GDK_BUTTON_PRESS);
	g_signal_connect (G_OBJECT (gResultsTable), "button_press_event", G_CALLBACK(_gui_tree_view_event_handler), NULL);
}

static gboolean _gui_quit (GtkWidget *widget, gpointer data)
{
	teleport_fini_main_loop ();

	return FALSE;
}

gboolean _gui_init (int *argc, char **argv[])
{
    gtk_init(argc, argv);
    
	GladeXML *pGladeXML = glade_xml_new (MAIN_WINDOW_GLADE_FILE, NULL, NULL);
	GtkWidget *pSearchButton;

	if (!pGladeXML)
	{
		g_print ("Unable to load glade file\n");
		return FALSE;
	}

	gMainWindow = glade_xml_get_widget (pGladeXML, "TestGui");
	gQueryWidget = glade_xml_get_widget (pGladeXML, "query_text");
	gResultsTable = glade_xml_get_widget (pGladeXML, "result_table");
	pSearchButton = glade_xml_get_widget (pGladeXML, "search_button");

	g_signal_connect (G_OBJECT (pSearchButton), "clicked", G_CALLBACK (_gui_search), NULL);
	g_signal_connect (G_OBJECT (gMainWindow), "destroy", G_CALLBACK (_gui_quit), NULL);

	_gui_init_tree_view ();

	if (!GTK_WIDGET_VISIBLE (gMainWindow))
		gtk_widget_show_all (gMainWindow);

	return TRUE;

}
