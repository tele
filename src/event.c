/**
*
* Teleport event management
*
* event.c
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#include "teleport-private.h"
#include "teleport.h"

static GMainLoop *teleportMainLoop = NULL;

void teleport_trigger_query_update (GSList* data)
{
	g_idle_add(_gui_update_list,(gpointer)data);

	/* TODO: add the items to a global list, so we can free them later on */
}

gboolean teleport_init_main_loop(void) 
{
	teleportMainLoop = g_main_loop_new (NULL, FALSE);

	g_main_loop_run (teleportMainLoop);
}

gboolean teleport_fini_main_loop(void)
{
	g_main_quit (teleportMainLoop);
}
