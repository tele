/**
*
* Teleport main file
*
* main.c
*
* Copyright : (C) 2007 by Diogo Ferreira <diogo@underdev.org>
*             (C) 2007 by João Oliveirinha <joliveirinha@floodbit.org>
*
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 3
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
**/

#include "teleport.h"
#include "teleport-private.h"

int main (int argc, char *argv[])
{
	_gui_init (&argc, &argv);

	teleport_load_plugin ("console");
/*	teleport_load_plugin ("tracker"); */
	teleport_load_plugin ("beagleplugin");

	teleport_init_main_loop ();

	return 0;
}
